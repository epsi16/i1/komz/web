# Stage 1
FROM node:10-alpine as build-step
RUN mkdir -p /app
WORKDIR /app
#COPY nginx.conf /etc/nginx/sites-enabled/
#COPY nginx.conf /etc/nginx/sites-available/
COPY package.json /app
ADD src /app/src
COPY angular.json /app
COPY tsconfig.json /app
COPY tsconfig.app.json /app



RUN npm i -g @angular/cli
RUN npm install
RUN npm run build --prod
# Stage 2
FROM nginx:1.17.1-alpine
#COPY /etc/nginx/sites-available/* /etc/nginx/sites-enabled/
COPY --from=build-step /app/dist/komz /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
