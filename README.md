# Web

The project has been generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.3.

## Installation
- `make install`

## Démarrage
- `make start`

## Arrêt
- `make stop`

## Supprimer le conteneur et ses images
- `make clean`

## Consulter les logs
- `make logs`

## Réccupérer l'IP du conteneur
- `make ip`
