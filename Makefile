clean:
	docker image rm komz-web:latest node:10-alpine emoovi/komz-web:latest
install:
	docker build -t emoovi/komz-web . && \
	docker-compose build && \
	npm install
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' komz-web
logs:
	docker-compose logs -f web
start:
	docker-compose up -d
stop:
	docker-compose down
