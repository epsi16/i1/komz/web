export const environment = {
  production: true,
  url_auth: 'http://51.158.98.220/',
  url_user: 'https://api-user.komz.lab-epsi.ovh/',
  url_post: 'https://api-post.komz.lab-epsi.ovh/',
  url_comment: 'https://api-comment.komz.lab-epsi.ovh/',
  url_like: 'https://api-like.komz.lab-epsi.ovh/'
};
