// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_auth: 'http://51.158.98.220/',
  url_user: 'https://api-user.komz.lab-epsi.ovh/',
  url_post: 'https://api-post.komz.lab-epsi.ovh/',
  url_comment: 'https://api-comment.komz.lab-epsi.ovh/',
  url_like: 'https://api-like.komz.lab-epsi.ovh/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
