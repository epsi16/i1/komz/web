import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login.component';
import { FeedComponent } from './component/feed.component';
import { InscriptionComponent } from './component/inscription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NavigationComponent } from './component/navigation.component';
import { PostComponent } from './component/post.component';
import { CommentComponent } from './component/comment.component';
import { ProfilComponent } from './component/profil.component';
import { EditProfilComponent } from './component/editprofil.component';
import { NotificationComponent } from './component/notification.component';
import { SearchComponent } from './component/search.component';
import { SubscribeComponent } from './component/subscribe.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import {initializeKeycloak} from './utility/app.init';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FeedComponent,
    InscriptionComponent,
    NavigationComponent,
    PostComponent,
    CommentComponent,
    ProfilComponent,
    EditProfilComponent,
    NotificationComponent,
    SearchComponent,
    SubscribeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    KeycloakAngularModule
  ],
  providers: [FeedComponent, CommentComponent, PostComponent, SearchComponent, SubscribeComponent, KeycloakService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },],
  bootstrap: [
    AppComponent,
    LoginComponent,
    FeedComponent,
    InscriptionComponent,
    NavigationComponent,
    PostComponent,
    CommentComponent,
    ProfilComponent,
    EditProfilComponent,
    NotificationComponent,
    SearchComponent,
    SubscribeComponent
  ]
})
export class AppModule { }
