import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  private url_user = environment.url_user;

  private POST_INSCRIPTION = this.url_user + "user";

  constructor(
    private httpClient: HttpClient) {
  }


  public uploadImage(ids) {
    return this.httpClient.post<JSON>(this.POST_INSCRIPTION, ids);
  }

  public signIn(ids) {
    return this.httpClient.post<JSON>(this.POST_INSCRIPTION, ids);
  }
}
