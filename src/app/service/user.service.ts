import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = environment.url_user;

  private GET_USER = this.url + "user/";

  private GET_USER_LIKE = this.url + "user/like/";

  private PUT_USER = this.url + "user";

  private IS_SUB = this.url + "user/issubscribeto/"

  private SUB = this.url + "user/subscribe/"

  private SUB_TO = this.url + "user/subscribeto/"

  private SUB_BY = this.url + "user/subscribeby/"

  private POST_USER = this.url + "user"

  constructor(
    private httpClient: HttpClient) {
  }




  public getUser(username) {
    return this.httpClient.get(this.GET_USER+ username);
  }

  public getUserLike(username) {
    return this.httpClient.get(this.GET_USER_LIKE + username);
  }

  public putUser(form) {
    return this.httpClient.put(this.PUT_USER, form);
  }

  public getIsSub(currentUser, user) {
    return this.httpClient.get(this.IS_SUB+ currentUser + "/" + user);
  }

  public sub(sub) {
    return this.httpClient.post<any>(this.SUB, sub);
  }

  public unSub(currentUser, user) {
    return this.httpClient.delete(this.SUB+ user + "/" + currentUser);
  }

  public getSubTo(user) {
    return this.httpClient.get(this.SUB_TO+ user + "/0/100" );
  }

  public getSubBy(user) {
    return this.httpClient.get(this.SUB_BY+ user + "/0/100" );
  }

  public postUser(user){
    return this.httpClient.post(this.POST_USER, user);
  }


}
