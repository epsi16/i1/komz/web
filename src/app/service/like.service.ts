import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  private url_user = environment.url_like;

  private POST_LIKE = this.url_user + "like";

  constructor(
    private httpClient: HttpClient) {
  }

  public like(like) {
    return this.httpClient.post<any>(this.POST_LIKE, like);
  }
}
