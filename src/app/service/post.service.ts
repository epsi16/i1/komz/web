import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { SendPost } from 'app/model/sendPostModel';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url = environment.url_post;

  
  private GET_FEED = this.url + "post/feed/";
  private GET_POST = this.url + "post/";
  private GET_POSTS_USER = this.url + "post/profile/";
  private POST_POST = this.url+ "post"

  constructor(
    private httpClient: HttpClient) {
  }


 


  public getUser(idUser, depart, nombre) {
    return this.httpClient.get(this.GET_FEED + idUser + "/" + depart + "/" + nombre);
  }

  public getPost(idPost, idCurrentUser) {
    return this.httpClient.get(this.GET_POST + idPost + "/" + idCurrentUser);
  }

  public getPostUser(idUser, curentUser, depart, nombre) {
    return this.httpClient.get(this.GET_POSTS_USER + idUser + "/" + curentUser + "/" + depart + "/" + nombre);
  }

  public postPost(post){
    
    return this.httpClient.post<any>(this.POST_POST, post);
  }
}
