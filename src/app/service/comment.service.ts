import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private url = environment.url_comment;

  
  private GET_LISTCOMMENT = this.url + "comment/post/";
  private POST_COMMENT = this.url + "comment"
  private GET_SOUSCOMMENT = this.url + "comment/child/"

  constructor(
    private httpClient: HttpClient) {
  }


  public getComments(idPost, depart, nombre) {
    return this.httpClient.get(this.GET_LISTCOMMENT + idPost + "/" + depart + "/" + nombre);
  }

  public postComment(comment){
    return this.httpClient.post<Comment>(this.POST_COMMENT, comment);
  }

  public getSousComment(idComment){
    return this.httpClient.get(this.GET_SOUSCOMMENT + idComment +"/0/50");
  }



}
