export class Post {

    public added: string;
    public boolean_parent: string;
    public content: string;
    public id: string;
    public id_parent: string;
    public link_image: string;
    public link_image_profile: string;
    public nb_comment: string;
    public nb_like: string;
    public nb_retweet: string;
    public updated: string;
    public username_user: string;
    public isLike: boolean;
    public parentContent: Post;
    public rtPar: String;
  
  }