export class User {

    public added: string;
    public biography: string;
    public boolean_reset_password: string;
    public count_subscribe_by: string;
    public count_subscribe_to: string;
    public first_name: string;
    public id: string;
    public last_name: string;
    public link_image_profile: string;
    public link_reset_password: string;
    public updated: string;
    public username: string;
  
  }