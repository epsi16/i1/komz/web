import { Component } from '@angular/core';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { User } from './model/userModel';
import { LoginService } from './service/login.service';
import { UserService } from './service/user.service';
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'komz';
  constructor(
    protected readonly keycloakAngular: KeycloakService,
    private loginService: LoginService,
    private userService: UserService
  ) {
  }
  userProfil;

  ngOnInit(): void
    {

      

        this.keycloakAngular
            .isLoggedIn()
            .then( loggedIn => {
                if( loggedIn ) {


                  this.keycloakAngular.loadUserProfile()
                  
                  .then(profile => {
                      this.userProfil = profile;
                  })
                  .catch( reason => {console.log( reason )});
          

                    this.userService.getUser(this.keycloakAngular.getUsername()).subscribe((data: User)  => {
                      sessionStorage.setItem("User", JSON.stringify(data));
                      
                      let user: User = <User> JSON.parse(sessionStorage.getItem("User"));
                
                    }, 
                    err => {
                      if(err.status == 404){
                        // var user = new User;
                        let formData = new FormData();
                        formData.append("first_name", this.userProfil.firstname);
                        formData.append("last_name", this.userProfil.lastname);
                        formData.append("username", this.userProfil.username);
                        formData.append("added", formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString());
                        
                        this.userService.postUser(formData).subscribe();
                        setTimeout(() => {
                        this.userService.getUser(this.keycloakAngular.getUsername()).subscribe((data: User)  => {
                          sessionStorage.setItem("User", JSON.stringify(data));
                          
                          let user: User = <User> JSON.parse(sessionStorage.getItem("User"));
                    
                        });
                      }, 400);
                      }
                    }
                    );
                }
            })
            .catch( reason => console.log ( reason ));
    }
}
