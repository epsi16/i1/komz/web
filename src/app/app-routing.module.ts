import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { EditProfilComponent } from './component/editprofil.component';
import { FeedComponent } from './component/feed.component';
import { InscriptionComponent } from './component/inscription.component';
import { LoginComponent } from './component/login.component';
import { NotificationComponent } from './component/notification.component';
import { PostComponent } from './component/post.component';
import { ProfilComponent } from './component/profil.component';
import { SearchComponent } from './component/search.component';
import { SubscribeComponent } from './component/subscribe.component';
import { AuthGuard } from './utility/app.guard';

const routes: Routes = [
  { path: 'app-root', component: AppComponent, canActivate: [AuthGuard] },
  { path: '', component: FeedComponent , canActivate: [AuthGuard]},
  { path: 'inscription', component: InscriptionComponent, canActivate: [AuthGuard] },
  { path: 'feed', component: FeedComponent, canActivate: [AuthGuard]},
  { path: 'post/:idpost', component: PostComponent, canActivate: [AuthGuard] },
  { path: 'profil/:idprofil', component: ProfilComponent, canActivate: [AuthGuard] },
  { path: 'editprofil/:idprofil', component: EditProfilComponent, canActivate: [AuthGuard] },
  { path: 'notification', component: NotificationComponent, canActivate: [AuthGuard]},
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard]},
  { path: 'sub/:typeSub', component: SubscribeComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
