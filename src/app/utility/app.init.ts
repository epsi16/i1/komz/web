import { KeycloakService } from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService) : () => Promise<boolean> {
    return () =>
      keycloak.init({
        config: {
          url: 'https://komz.keycloak.lab-epsi.ovh/auth',
          realm: 'Komz',
          clientId: 'komzweb',
        },initOptions: {
          onLoad: 'login-required',
          checkLoginIframe: false
      },
      enableBearerInterceptor: true,
      loadUserProfileAtStartUp: true,
      bearerExcludedUrls: ['/assets']
      
      });
  }