import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { InscriptionService } from 'app/service/inscription.service';

@Component({
  selector: 'inscription',
  templateUrl: '../view/inscription.component.html',
  styleUrls: ['../style/inscription.component.scss', '../style/login.component.scss']
})
export class InscriptionComponent implements OnInit {

  signInForm: FormGroup;
  public imagePath;
  imgURL: any;
  public message: string;

  constructor(
    private formBuilder: FormBuilder,
    private inscriptionService: InscriptionService
  ) { }

  ngOnInit() {
    this.imgURL = 'assets/images/profil-pict.png';
    this.signInForm = this.formBuilder.group({
      username: null,
      first_name: null,
      last_name: null,
      email: null,
      password: null,
      link_image_profile: null,
      added: null,
    });
  }


  onImageSelect(files) {
    if (files.length === 0) {
      return;
    }

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }

  signIn(data) {
    data.link_image_profile;
    let formData = new FormData();
    formData.append('file', data.link_image_profile);
    var image_url;
    // this.inscriptionService.uploadImage(formData).subscribe(
    //   (res) => {
    //     image_url = res;
    //   },
    //   () => {

    //   }
    // );

    let ids = {
      "username": data.username,
      "first_name": data.first_name,
      "last_name": data.last_name,
      "email": data.email,
      "password": data.password,
      "link_image_profile": image_url,
      "added": new Date()
    }
  };
}



