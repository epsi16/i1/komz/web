import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LoginService } from 'app/service/login.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { log } from 'console';
import {User} from '../model/userModel'
import { UserService } from 'app/service/user.service';


@Component({
  selector: 'subs',
  templateUrl: '../view/subscribe.component.html',
  styleUrls: ['../style/subscribe.component.scss'],
})
export class SubscribeComponent implements OnInit {

  userList = [];
  typePage; 
  typePageAffiche;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {this.route.params.subscribe(params => this.typePage = params.typeSub);
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  }

  ngOnInit() {
    if(this.typePage == "subscribeTo"){
      this.typePageAffiche = "Abonnements";
      this.userService.getSubTo(JSON.parse(sessionStorage.getItem("User")).id).subscribe((data: User[])  => {
        this.userList = data;
      });
    }else{
      this.typePageAffiche = "Abonnées";

      this.userService.getSubBy(JSON.parse(sessionStorage.getItem("User")).id).subscribe((data: User[])  => {
        this.userList = data;
      });
    }
  }

  serchPerson(event){
    var like = event.target.value;
    this.userService.getUserLike(like).subscribe((data: User[])  => {
      this.userList = data;
    });
  }

  toProfil(username){
    this.router.navigate([username]);
  }

  destroyComponent() {
    this.router.navigate(["profil/" + JSON.parse(sessionStorage.getItem("User")).username]);
  }


}
