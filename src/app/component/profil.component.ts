import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Like } from 'app/model/likeModel';
import { Post } from 'app/model/postModel';
import { Sub } from 'app/model/subModel';
import { User } from 'app/model/userModel';
import { LikeService } from 'app/service/like.service';
import { PostService } from 'app/service/post.service';
import { UserService } from 'app/service/user.service';
import { element } from 'protractor';


@Component({
  selector: 'profil',
  templateUrl: '../view/profil.component.html',
  styleUrls: ['../style/profil.component.scss']
})

export class ProfilComponent implements OnInit {

  user = new User;
  userName;
  isPostToComment = true;
  posts = [];
  userLocal;
  userTemp ="";
  postsBrut = [];
  postNonTri =[];
  isSub;

  commentaireToComment = {
    "id": null,
    "content": null,
    "link_image": null,
    "id_parent": null,
    "id_post": null,
    "username_user": null,
    "user_image": null,
    "added": null
  };



  postsliked = [{
    "idpost": "123",
    "content": "je fais un post lolilol",
    "username_user": "Ponce",
    "added": "9 dec 15:02",
    "nbr_repost": 24,
    "nbrlike": 125,
    "link_image": "https://pbs.twimg.com/media/EEQKmV8WwAAPDVS?format=jpg&name=900x900",
    "user_image": "https://pbs.twimg.com/profile_images/1317204195934670851/XI9VFwc-_400x400.jpg"
  },
  {
    "idpost": "124",
    "content": "c'est un post aussi mdr !",
    "username_user": "Ponce",
    "added": "9 dec 15:20",
    "nbr_repost": 52,
    "nbrlike": 326,
    "link_image": null,
    "user_image": "https://pbs.twimg.com/profile_images/1317204195934670851/XI9VFwc-_400x400.jpg"
  }]

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private postService: PostService,
    private likeService: LikeService
  ) {this.route.params.subscribe(params => this.userName = params.idprofil);
    this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  ngOnInit() {
    
    this.userLocal = <User> JSON.parse(sessionStorage.getItem("User"));
    
    this.userService.getUser(this.userName).subscribe((data: User)  => {
      this.user = data;
      this.userService.getIsSub(this.userLocal.id, data.id).subscribe((data: boolean)  => {
        this.isSub = data;
  });
      
    });
    this.postService.getPostUser(this.userName, this.userLocal.id, 0, 100).subscribe((data: Post[])  => {
      data.forEach(element => {
        if(element.id_parent != null){
          element.parentContent.updated =  formatDate(new Date(element.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
          this.posts.push(element.parentContent)
        }else{
          element.updated = formatDate(new Date(element.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
          this.posts.push(element)
        }
      });
});
  }

  onClickPost(idpost) {
    this.router.navigate(['post', idpost]);
  }

  saveIdPost(post) {
    this.setComment(post);
  }

  setComment(post) {
    this.isPostToComment= true;
    this.commentaireToComment.content = post.content;
    this.commentaireToComment.user_image = post.link_image_profile;
    this.commentaireToComment.username_user =  post.username_user;
    this.commentaireToComment.id_post = post.id;
  }


  clickEdit(){
    this.router.navigate(['editprofil', this.userName]);
  }

  subUnsub(){
    var sub = new Sub();
    sub.id_user = this.userLocal.id;
    sub.id_subscribe = this.user.id;
    sub.added = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en').toString();
    if(this.isSub == false){
      this.userService.sub(sub).subscribe(
        () => {
          this.isSub = !this.isSub;
        },() => {
        }
      )
    }else{
      this.userService.unSub(this.user.id, this.userLocal.id).subscribe(
        () => {
          this.isSub = !this.isSub;

        },() => {
        }
      )
    }
  }

  toProfil(page){
    this.router.navigate([page]);
  }

  subBy(){
    this.router.navigate(['sub', "subscribeBy"]);
  }

  subTo(){
    this.router.navigate(['sub', "subscribeTo"]);
  }
  
  like(post){
    if(post.isLike == false){
      post.isLike = true;
      post.nb_like += 1;
      var like = new Like();
      like.added = formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString();
      like.id_user = this.user.id;
      like.id_entity = post.id;
      like.type = "post";
      this.likeService.like(like).subscribe(
        () => {
          console.log("like");
        },() => {
          console.log("problème lors du like");
        }
      )
    }
    
  }

  repost(post){
    
    let formData = new FormData();
    formData.append("added", formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString());
    formData.append("content",null);
    formData.append("id_parent",post.id);
    formData.append("image",null);
    formData.append("username_user",this.userLocal.username);
    this.postService.postPost(formData).subscribe(
      () => {
        post.nb_retweet +=1;
        console.log("repost");
      },() => {
        console.log("problème lors du repost");
      }
    )
  }
}
