import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedComponent } from './feed.component';
import { formatDate, Location } from '@angular/common';
import { element } from 'protractor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PostService } from 'app/service/post.service';
import { CommentService } from 'app/service/comment.service';
import { Post } from 'app/model/postModel';
import { User } from 'app/model/userModel';
import { Like } from 'app/model/likeModel';
import { LikeService } from 'app/service/like.service';

@Component({
  selector: 'post',
  templateUrl: '../view/post.component.html',
  styleUrls: ['../style/post.component.scss', '../style/feed.component.scss']
})
export class PostComponent implements OnInit {

  idpost;
  imageToDisplay;
  comments;
  post = new Post;
  isPostToComment= true;
  commentPost = [];
  commentaireToComment = {
    "id": null,
    "content": null,
    "link_image": null,
    "id_parent": null,
    "id_post": null,
    "username_user": null,
    "user_image": null,
    "added": null
  };
  commentSansSousComment;

  sendMessageForm: FormGroup;
  userLocal;

  constructor(
    private route: ActivatedRoute,
    private feed: FeedComponent,
    private host: ElementRef<HTMLElement>,
    private router: Router,
    private _location: Location,
    private formBuilder: FormBuilder,
    private postService: PostService,
    private commentService: CommentService,
    private likeService: LikeService
  ) {
    this.route.params.subscribe(params => this.idpost = params.idpost);
  }

  ngOnInit() {
    this.userLocal = <User> JSON.parse(sessionStorage.getItem("User"));

    this.sendMessageForm = this.formBuilder.group({
      content: null
    });

      this.postService.getPost(this.idpost, JSON.parse(sessionStorage.getItem("User")).id).subscribe((data: Post)  => {
        this.post = data;
        this.post.updated = formatDate(new Date(this.post.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
          
        });
    

    this.getComment();


  }

  getCurrentUser(){
    
  }

  getComment(){
    this.commentService.getComments(this.idpost, 0,100).subscribe((data: any)  => {
      this.comments = data;
      this.commentSansSousComment = data;
      var commentList =[];
      data.forEach(element => {
        var elem = element;
        if(element.count_child != 0){
          this.commentService.getSousComment(elem.id).subscribe((data2: any)  => {
            elem.sousComment = data2;
            elem.sousComment.forEach(elementSC => {
              elementSC.updated =  formatDate(new Date(elementSC.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
            });
            
            
          });
        }else{
          elem.sousComment = [];
        }
        elem.updated = formatDate(new Date(elem.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
        commentList.push(elem);
      });
      this.comments = commentList;
      console.log(this.comments)
    });
    
    
  }

  toProfil(page){
    this.router.navigate([page]);
  }


  destroyComponent() {
    this._location.back();
  }

  saveIdComment(comment, post) {
    this.commentaireToComment.content = comment.content;
    this.commentaireToComment.user_image = comment.link_user_image;
    this.commentaireToComment.username_user = comment.username_user;
    this.commentaireToComment.id_parent = comment.id;
    this.commentaireToComment.id_post = post.id;
  }

  saveIdPost() {
    this.isPostToComment= true;
    this.commentaireToComment.content = this.post.content;
    this.commentaireToComment.user_image = this.post.link_image_profile;
    this.commentaireToComment.username_user =  this.post.username_user;
    this.commentaireToComment.id_post = this.post.id;
  }



  closeImage(){
    document.getElementById("modalImage").style.display = "none";
  }

  displayImage(url){
    document.getElementById("modalImage").style.display = "block";
    this.imageToDisplay = url;
  }

  like(post){
    if(post.isLike == false){
      post.isLike = true;
      post.nb_like += 1;
      var like = new Like();
      like.added = formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString();
      like.id_user = this.userLocal.id;
      like.id_entity = post.id;
      like.type = "post";
      this.likeService.like(like).subscribe(
        () => {
          console.log("like");
        },() => {
          console.log("problème lors du like");
        }
      )
    }
    
  }

  repost(post){
    
    let formData = new FormData();
    formData.append("added", formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString());
    formData.append("content",null);
    formData.append("id_parent",post.id);
    formData.append("image",null);
    formData.append("username_user",this.userLocal.username);
    this.postService.postPost(formData).subscribe(
      () => {
        post.nb_retweet += 1;
        console.log("repost");
      },() => {
        console.log("problème lors du repost");
      }
    )
  }
  


}
