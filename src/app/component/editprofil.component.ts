import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'app/model/userModel';
import {formatDate} from '@angular/common';
import { UserService } from 'app/service/user.service';


@Component({
    selector: 'editprofil',
    templateUrl: '../view/editprofil.component.html',
    styleUrls: ['../style/editprofil.component.scss']
})

export class EditProfilComponent implements OnInit {
    user: User;

    editProfilForm: FormGroup;

    userName;
    imgprofil;
    public imagePath;
    imgURL: any;
    public message: string;
    profilUser = {
        "username": "Croumy",
        "biography": "Yo les gens PonceVie et DoctolibVie Tu peux pas me test avec mon col roulé..",
        "usernlink_image_profil": "https://pbs.twimg.com/profile_images/1223664963203538944/eL1JGdhk_400x400.jpg",
        "email": "clemence.roumy@epsi.fr",
        "abonne": 2,
        "abonnement": 82
    }



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService
    ) { this.route.params.subscribe(params => this.userName = params.idprofil); }

    ngOnInit() {

        this.user = <User>JSON.parse(sessionStorage.getItem("User"));
        this.imgprofil = this.user.link_image_profile;



        this.editProfilForm = this.formBuilder.group({
            id: this.user.id,
            biography: this.user.biography,
            first_name: this.user.first_name,
            last_name: this.user.last_name,
            updated: formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en').toString(),
            image: ['']
        });

    }

    clickEdit() {
        this.router.navigate(['post', this.userName]);
    }

    onImageSelect(files, event) {
        if (files.length === 0) {
            return;
        }

        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = 'Only images are supported.';
            return;
        }

        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgprofil = reader.result;
        };

        if (event.target.files.length > 0) {
            const files = event.target.files[0];
            this.editProfilForm.get('image').setValue(files);
          }
    }

    editProfil() {
        let formData = new FormData();
        formData.append("id", this.editProfilForm.get("id").value);
        formData.append("biography", this.editProfilForm.get("biography").value);
        formData.append("first_name", this.editProfilForm.get("first_name").value);
        formData.append("last_name", this.editProfilForm.get("last_name").value);
        formData.append("updated", this.editProfilForm.get("updated").value);
        formData.append("image", this.editProfilForm.get("image").value);
        this.userService.putUser(formData).subscribe(
            () => {
              console.log("Utilisateur modifié");
              this.router.navigate(["profil/" + this.user.username]);
            },() => {
              console.log("problème lors de la modification de l'utilisateur");
            }
          )

    }

    

}
