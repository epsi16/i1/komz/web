import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CommentComponent } from './comment.component';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from 'app/model/userModel';
import { PostService } from 'app/service/post.service';
import { Post } from 'app/model/postModel';
import { SendPost } from 'app/model/sendPostModel';
import { cpuUsage, send } from 'process';
import {formatDate} from '@angular/common';
import { Like } from 'app/model/likeModel';
import { LikeService } from 'app/service/like.service';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'feed',
  templateUrl: '../view/feed.component.html',
  styleUrls: ['../style/feed.component.scss']
})
export class FeedComponent implements OnInit {

  public posts = [];
  public message: string;
  public imagePath;
  imgURL: any;
  sendMessageForm: FormGroup;
  user = <User> JSON.parse(sessionStorage.getItem("User"));
  imgUser;
  imageToDisplay; 
  sendPost:SendPost;
  image:File;
  file: File; 

  showEmojiPicker = false;

  set = 'twitter';
  isPostToComment = true;
  commentPost = [];
  commentaireToComment = {
    "id": null,
    "content": null,
    "link_image": null,
    "id_parent": null,
    "id_post": null,
    "username_user": null,
    "user_image": null,
    "added": null
  };

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private postService: PostService, 
    private likeService: LikeService
  ) {this.sendMessageForm = this.formBuilder.group({
    added: formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString(),
    content: null,
    id_parent: null,
    link_image: [''],
    username_user: null,
  }); 
}

  ngOnInit() {
    setTimeout(() => {
      this.user = <User> JSON.parse(sessionStorage.getItem("User"));
    
      this.imgUser = JSON.parse(sessionStorage.getItem("User")).link_image_profile;

      this.getPost();
  
      this.sendPost = new SendPost();
     }, 1000);
    
     

   
    
  }

  getPost(){
    this.posts = [];
    this.postService.getUser(JSON.parse(sessionStorage.getItem("User")).id,0,100).subscribe((data: Post[])  => {
  
      data.forEach(element => {
        if(element.id_parent != null){
          element.parentContent.updated =  formatDate(new Date(element.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
          element.parentContent.rtPar = element.username_user
          this.posts.push(element.parentContent)
        }else{
          element.updated = formatDate(new Date(element.updated), 'dd/MM/yyyy HH:mm:ss', 'en').toString();
          this.posts.push(element)
        }
      });
    });
  }
  

  postPost(){

    let formData = new FormData();
    formData.append("added",this.sendMessageForm.get("added").value);
    formData.append("content",this.sendMessageForm.get("content").value);
    formData.append("id_parent",this.sendMessageForm.get("id_parent").value);
    formData.append("image",this.sendMessageForm.get("link_image").value);
    formData.append("username_user",JSON.parse(sessionStorage.getItem("User")).username);
    this.postService.postPost(formData).subscribe(
      () => {
        console.log("Post posté :)");
        this.getPost();
      },() => {
        console.log("post pas posté");
      }
    )

  }

  repost(post){
    
    let formData = new FormData();
    formData.append("added",this.sendMessageForm.get("added").value);
    formData.append("content",null);
    formData.append("id_parent",post.id);
    formData.append("image",null);
    formData.append("username_user",JSON.parse(sessionStorage.getItem("User")).username);
    this.postService.postPost(formData).subscribe(
      () => {
        console.log("repost");
        post.nb_retweet += 1; 
      },() => {
        console.log("problème lors du repost");
      }
    )
  }


  onFocus() {
    this.showEmojiPicker = false;
  }


  onClickPost(idpost) {
    this.router.navigate(['post', idpost]);
  }

  onImageSelect(files, event) {
    if (files.length === 0) {
      return;
    }

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.sendMessageForm.get('link_image').setValue(file);
    }

  }

  saveIdPost(post) {
    this.setComment(post);
  }

  setComment(post) {
    this.isPostToComment = true;
    this.commentaireToComment.content = post.content;
    this.commentaireToComment.user_image = post.link_image_profile;
    this.commentaireToComment.username_user = post.username_user;
    this.commentaireToComment.id_parent = post.id_parent;
    this.commentaireToComment.id_post = post.id;
  }

  toProfil(page){
    this.router.navigate([page]);
  }


  

  closeImage(){
    document.getElementById("modalImage").style.display = "none";
  }

  displayImage(url){
    document.getElementById("modalImage").style.display = "block";
    this.imageToDisplay = url;
  }


  like(post){
    if(post.isLike == false){
      post.isLike = !post.isLike; 
      var like = new Like();
      like.added = formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString();
      like.id_user = this.user.id;
      like.id_entity = post.id;
      like.type = "post";
      this.likeService.like(like).subscribe(
        () => {
          console.log("post liké");
        },() => {
          console.log("problème lors du like");
        }
      )
    }
    }
}
