import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LoginService } from 'app/service/login.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { log } from 'console';
import {User} from '../model/userModel'
import { UserService } from 'app/service/user.service';


@Component({
  selector: 'search',
  templateUrl: '../view/search.component.html',
  styleUrls: ['../style/search.component.scss'],
})
export class SearchComponent implements OnInit {

  userList = [];

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
   
  }

  serchPerson(event){
    var like = event.target.value;
    this.userService.getUserLike(like).subscribe((data: User[])  => {
      this.userList = data;
      
    },(err) => {
      if(err.status == 404){
        this.userList = [];
      }
    });
  }

  toProfil(username){
    this.router.navigate([username]);
  }

  destroyComponent() {
    this.router.navigate(["feed"]);
  }


}
