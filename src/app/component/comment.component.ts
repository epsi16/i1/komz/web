import {Component, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Commentaire } from 'app/model/commentModel';
import { User } from 'app/model/userModel';
import { CommentService } from 'app/service/comment.service';
import {formatDate} from '@angular/common';
import { PostComponent } from './post.component';

@Component({
  selector: 'createComment',
  templateUrl: '../view/comment.component.html',
  styleUrls: ['../style/comment.component.scss']
})
export class CommentComponent implements OnInit {
    @Input() commentaireToComment;

    content: SafeHtml;
    sendMessageForm: FormGroup;
    isPostToComment = false;
    comment: Commentaire;
    user:User;
    imgURL: any;
    public imagePath;


    constructor(
        private formBuilder: FormBuilder,
        private commentService: CommentService,
        private postComponent: PostComponent
      ) { }

      ngOnInit(){
        this.sendMessageForm = this.formBuilder.group({
            content: null
          });

      this.user = <User> JSON.parse(sessionStorage.getItem("User"));


    }

    onSubmitComment(data) {
        
        
        this.comment = new Commentaire();
        this.comment.content = data.content;
        this.comment.id_post = this.commentaireToComment.id_post;
        this.comment.username_user = this.user.username;
        this.comment.id_parent = this.commentaireToComment.id_parent;
        this.comment.added = formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en').toString();
        this.commentService.postComment(this.comment).subscribe(
          () => {
            document.getElementById('closeModal').click();
            this.postComponent.getComment();
          },() => {
            console.log("Problème lors de l'envoie du commentaire");
          }
        )
      }

      onImageSelect(files, event) {
        if (files.length === 0) {
          return;
        }
    
        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {;
          return;
        }
    
        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
          this.imgURL = reader.result;
        };
        if (event.target.files.length > 0) {
          const file = event.target.files[0];
          this.sendMessageForm.get('link_image').setValue(file);
        }
    
      }
}
