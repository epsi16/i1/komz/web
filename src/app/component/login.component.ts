import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LoginService } from 'app/service/login.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { log } from 'console';
import {User} from '../model/userModel'


@Component({
  selector: '',
  templateUrl: '../view/login.component.html',
  styleUrls: ['../style/login.component.scss'],
})
export class LoginComponent implements OnInit {

  uploadForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.uploadForm = this.formBuilder.group({
      email: null,
      password: null
    });

  }

  



}
