import {Component, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'imageComment',
  templateUrl: '../view/comment.component.html',
  styleUrls: ['../style/comment.component.scss']
})
export class ImageComponent implements OnInit {
    @Input() commentaireToComment;

    content: SafeHtml;
    sendMessageForm: FormGroup;
    isPostToComment = false;


    constructor(
        private formBuilder: FormBuilder,
      ) { }

      ngOnInit(){
        this.sendMessageForm = this.formBuilder.group({
            content: null
          });


    }

}
