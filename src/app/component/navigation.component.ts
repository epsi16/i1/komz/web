import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/model/userModel';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'navigation',
  templateUrl: '../view/navigation.component.html',
  styleUrls: ['../style/navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  posts;
  user;

  constructor(
    private router: Router,
    protected readonly keycloakAngular: KeycloakService,
  ) { }

  ngOnInit() {
    this.user = <User> JSON.parse(sessionStorage.getItem("User"));
  }

  navigate(page){
    if(page == 'deco'){
      this.keycloakAngular.logout()
    }else{
      this.router.navigate([page]);
      
    }
  }
}
